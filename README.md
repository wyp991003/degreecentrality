**Experimental evaluation in the article Semantic "Centrality for Temporal Graphs" (ABDIS 2023)**

/!\ The personal information of this gitlab account is temporarily anonymous during the process review of the article in ADBIS 2023. It is not possible to contact this account until the end of the process review. /!\

***Abstract of the paper***

Centrality metrics in graphs, such as degree, help to understand the influence of entities in various applications. They are used to quantify the influence of entities based on their relationships. Time dimension has been integrated to take into account the evolution of relationships between entities in real-world phenomenon. For instance, in the context of disease spreading, new contacts may appear and disappear over time between individuals. However, they do not take into account the semantics of entities and their relationships. For example, in the context of a disease spreading, some relationships (such as physical contacts) may be more important than others (such as virtual contacts). To overcome this drawback, we propose centrality metrics, which are based on the classic degree metric and integrate both temporal aspects and semantics of entities and relationships. Moreover, we propose to exploit these centrality metrics according to several axes: the time dimension (a time point or interval), the semantics of entity or relationships as well as the centrality subject (a single entity or an entity class). We carry out several experimental assessments, with real-world datasets, to illustrate the efficiency and the usability of our solution. 

Keywords: Centrality Metrics, Degree, Influential Entities, Temporal Graphs.

***Summary of the experimental evaluation***

Our experimental assessment aims at (i) evaluating the efficiency of our proposal, and (ii) evaluating the usability of the proposed centrality metrics in a real application.

***Technical environment***

The experiments are conducted on a PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. One virtual machine installed on this hardware, that has 32 GB in terms of RAM and 100 GB in terms of disk size, is used for our experiments. The Neo4j graph database is installed in the virtual machine to store and query temporal graph datasets. To avoid any bias in the disk management, the default tuning of Neo4j is kept, no customized optimization techniques are used. The programming language used to implement our metrics is Python 3.7.

_Install your technical environment:_
1) Install Neo4j Server.
2) Install Pycharm with the Jupyter Notebook Plugin.
3) Open Pycharm.
4) Clone this remote repository in Pycharm. It will create a pycharm project. 


***Datasets***

We used two datasets entitled "Social experiment" and "E-commerce" presented in the [DKE gitlab](https://gitlab.com/2573869/dke_temporal_graph_experiments). 

We transformed each dataset into a temporal graph model presented. All transformation details are available in [DKE gitlab](https://gitlab.com/2573869/dke_temporal_graph_experiments).

Then, we imported the datasets in Neo4j.  All importation details are available in [DKE gitlab](https://gitlab.com/2573869/dke_temporal_graph_experiments).


***Efficiency Evaluation Methodogology***

We evaluate the computation time of the basic application of centrality metrics: ranking entities of a dataset for a chosen time interval. In our case, we choose to compute the average semantic temporal degree metric for each entity of each dataset. 

For this metric, we choose to put the parameters that make the computation the longest: we integrate all relationship labels and the whole time span of each entity. 

We define 5 scale factors by varying the number of edges involved in the metric calculation. From 1 to 5, the scale factor is 20%, 40%, 60%, 80%, 100% of the number of edges of each dataset respectively. We run the metric calculation 10 times for each scale factor and make the average elapsed computation time over the 10 executions. 

For the Social Experiment dataset, we can find the python code to automatically the previous process in the file [TestSocialExperiment.py](https://gitlab.com/2573869/degreecentrality/-/blob/main/test_efficiency/TestSocialExperiment.py). For the E-commerce dataset, we can find the python code to automatically the previous process in the file [TestECommerce.py
](https://gitlab.com/2573869/degreecentrality/-/blob/main/test_efficiency/TestECommerce.py).

To sum up, our experiments account for a total of 100 executions: 2 datasets x  5 scale factors x 10 executions. The results of our experiments are presented in the Figure [exp_efficiency](https://gitlab.com/2573869/degreecentrality/-/blob/main/results_efficiency/exp_efficiency.PNG).

***Usability Evaluation Methodogology***

We evaluate the usability of the metrics we propose. The goal is to show the importance of having temporal and semantic aspects for calculating centrality in a real application. We consider the analysis of the Social Experiment dataset. The goal of the analysis is to identify the students and the interactions that could play a key role in the epidemiological contagion on a period of 6 days. 

Here is the analysis: 
First, we want to analyse the global trends of students' interactions in the dataset to identify which interaction type(s) is the most interesting for the contagion study. To do so, we compute the semantic temporal degree distribution for each interaction type over the 6 days to identify the interaction type(s) with the highest metric values. The result is presented in the figure [here](https://gitlab.com/2573869/degreecentrality/-/blob/main/results_usability/exp_distribution.pdf).

Second, we want to identify the top-5 students with the highest number of interactions over the period of interest. Following the previous observation, we can focus our study on proximity interactions. So, we compute the average semantic temporal degree for each student over the 6 days and rank them. We position the resulting top-5 students on the previous proximity distribution to evaluate their relative importance. The result is presented by arrows in the figure [here](https://gitlab.com/2573869/degreecentrality/-/blob/main/results_usability/exp_distribution.pdf).

Thrid, we want to identify the specific days over the period in which the top-5 students have the most interactions. This may indicate the days at higher risks or outbreaks of the contagion. To do so, we compute the semantic temporal degree for each student ranked in the top-5 to observe the evolution of their interactions over time.  The result is presented by arrows in the figure [here](https://gitlab.com/2573869/degreecentrality/-/blob/main/results_usability/exp_evolution.pdf).

We can find the python code for this analysis in the file [DegreeCentrality_test_usability.ipynb](https://gitlab.com/2573869/degreecentrality/-/blob/main/test_usability/DegreeCentrality_test_usability.ipynb) that was used to produce the results of the analysis.

***Complementary Analyses for the Usability Evaluation***

We propose complementary analyses of our centrality metrics. 

We can find the python code for this analysis in the file [xxx]() that was used to produce the results of the previous analyses.

